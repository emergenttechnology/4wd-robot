# 4-Wheel-Drive Robot for common chassis #

## What is it? ##

This project creates 4WD Robot using ultrasonics for obstacle detection and avoidance.  It is .powered by an Arduino Mega2650, and relies on commonly available components.

It can make a complete machine on it's own, but for the most part I am using it to test subsystems for my "Rover" project.  Still, if it helps you out, Enjoy!

## How do I get set up? ##

### Shopping List ###

* 1 x Arduino Mega2560 or 100% clone.
* 1 x 4-Channel or 2 x 2-Channel L398 H-Bridge motor drivers.
* 1 x 4WD Motor Chassis (Jaycar, Polu, AliExpress, etc.
* 1 x HC-SR02 Ultrasonic ranging module and mounting bracket to suit.
* 1 x Small 5V Servo Motor
* Batteries.  I used 2 x 6-cell 'AA' packs, and these last very well.
* 1 x DPDT switch as a master power switch.

### Options ###

* 2 x NRF24L01 series transceiver module
* 1 x 3K0, 1 x 1K0 resistors for battery monitoring
* 1 x ACS712 current sensor for battery monitoring

### Contribution guidelines ###

This is very much a work in progress, and your feedback and suggestions are much appreciated.  Email is best way to send them.  chris DOT moran AT emergent DOT com DOT au

