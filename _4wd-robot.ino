// 4WD Robot using ultrasonics for obstacle detection and avoidance.
// Chris Moran (chris DOT moran AT emergent DOT com DOT au
//
// This project is designed to suit the common 4WD Robot Chassis available from
// a variety of resellers.  It can make a complete machine on it's own, but for
// the most part I am using it to test subsystems for my "Rover" project.
//
// Pins assume Arduino Mega 2560

// This code is an amalgamation of various snippets, and no ownership is claimed.
// In return, it is placed in the public domain for free and unrestricted usage
// (Although I would prefer NOT to see weapons systems using this code)

// Warning:
// If you define BURN_IN, the machine will not do
// anything remotely useful, except for cycling motors
// #define BURN_IN

#include "features.h"
#include "rover_hw.h"

#include <NewPing.h>
#include <Servo.h>

#ifdef OLED
#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#endif

#ifdef NRF24L01
#include <nRF24L01.h>
#include <RF24.h>
#endif


// Initialise the sonar module
NewPing sonar(TRIG_PIN, ECHO_PIN, MAX_DISTANCE);

#ifdef OLED
#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);
#endif

#ifdef NRF24L01
// Initialise the radio module
RF24 radio(RADIO_CE, RADIO_CSN);
const uint64_t pipes[2] = { 0xF0F0F0F0E1LL, 0xF0F0F0F0D2LL };
#endif

// Create a servo control
Servo myservo;

// Distances on either side
int leftDistance;
int rightDistance;
int currentDist = 0;

// "Road Speed"
int speedSet = 0;

bool status = true;

void setup() {
#ifdef DEBUG
  Serial.begin(115200);
#endif  
  pinMode(MOTOR_A_ENABLE, OUTPUT);
  pinMode(MOTOR_B_ENABLE, OUTPUT);
  pinMode(MOTOR_C_ENABLE, OUTPUT);
  pinMode(MOTOR_D_ENABLE, OUTPUT);
  pinMode(MOTOR_A_IN_1, OUTPUT);
  pinMode(MOTOR_A_IN_2, OUTPUT);
  pinMode(MOTOR_B_IN_1, OUTPUT);
  pinMode(MOTOR_B_IN_1, OUTPUT);
  pinMode(MOTOR_C_IN_1, OUTPUT);
  pinMode(MOTOR_C_IN_1, OUTPUT);
  pinMode(MOTOR_D_IN_1, OUTPUT);
  pinMode(MOTOR_D_IN_1, OUTPUT);
  pinMode(LED_BUILTIN, OUTPUT);
#ifdef DEBUG
  Serial.println("Ports set");
#endif

#ifdef OLED
#ifdef DEBUG
  Serial.println("OLED Boot");
#endif
  // Power up the OLED Display
  display.begin(SSD1306_SWITCHCAPVCC, 0x3C);  // initialize with the I2C addr 0x3C (for the 128x32)
  display.display();
  display.clearDisplay();
  display.setTextSize(2);
  display.setTextColor(WHITE);
  display.setCursor(0,0);
  display.print("4WD Chassis");
  display.display();
#endif

  myservo.attach(SERVO_PIN);
#ifdef OLED
  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(0,0);
  display.print("Servo Test");
  display.display();
#endif
  myservo.write(45);
  delay(500);
  myservo.write(135);
  delay(500);
  myservo.write(90);          // tells the servo to position at 90-degrees
  delay(1000);
#ifdef OLED
  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(0,0);
  display.print("Go!");
  display.display();
#endif
}

void loop() {
#ifdef BURN_IN
  while(1) {
    cycleServo();    
  }
#endif
  digitalWrite(LED_BUILTIN, status);
  status = !status;

  // Eyes forward
  myservo.write(90);
  delay(100);

  currentDist = getDistance();
  if(currentDist < COLL_DIST) {
    changeDirection();
  }
  forward();
  delay(500);
}

// Read distance from the ultrasonic sensor
int getDistance() {
  unsigned int uS = 0;
  int cm = 0;
  delay(70);   
  uS = sonar.ping();
  cm = uS / US_ROUNDTRIP_CM;
#ifdef OLED
  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(0,0);
  display.print("Dist: " + String(cm));
  display.display();
#endif
  return cm;
}

void changeDirection() {
  rightDistance = 0;
  leftDistance = 0;

  // stop forward movement
  stop();
#ifdef DEBUG
  Serial.println("Move Servo to 36");
#endif
  myservo.write(36);  
    delay(500);
    //get left distance
    leftDistance = getDistance();
#ifdef DEBUG
  Serial.println("Move Servo to 144");
#endif
    myservo.write(144);  
    delay(500);
    // get right distance
    rightDistance = getDistance();
    //return to centre
#ifdef DEBUG
  Serial.println("Move Servo to 90");
#endif
    myservo.write(90); 
    delay(500);
    compareDistance();
  }

// Find the greatest distance (left or right)
// and make an appropriate turn to avoid it
void compareDistance()
{
  if (leftDistance > rightDistance)
  {
    leftTurn();
  }
  else if (rightDistance > leftDistance)
  {
    rightTurn();
  }
   else //if they are equally obstructed
  {
    turnAround();
  }
}

// Execute a right turn
void rightTurn() {
#ifdef DEBUG
  Serial.println("rightTurn()");
#endif

#ifdef OLED
  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(0,0);
  display.print("Right!");
  display.display();
#endif
  motorBForward();
  motorDForward();
  motorAReverse();
  motorCReverse();
  motorASpeed(speedSet+MAX_SPEED_OFFSET);      
  motorCSpeed(speedSet+MAX_SPEED_OFFSET);


  // run motors this way for 1500mS
  delay(1500);
  stop();

  motorBForward();
  motorDForward();
  motorAForward();
  motorCForward();
} 

//Execute a left turn
void leftTurn() {
#ifdef DEBUG
  Serial.println("leftTurn()");
#endif

#ifdef OLED
  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(0,0);
  display.print("Left!");
  display.display();
#endif

  motorBReverse();
  motorDReverse();
  motorAForward();
  motorAForward();
  motorBSpeed(speedSet+MAX_SPEED_OFFSET);     
  motorDSpeed(speedSet+MAX_SPEED_OFFSET);    
  
  // run motors this way for 1500mS
  delay(1500);
  stop();

  motorBForward();
  motorDForward();
  motorAForward();
  motorCForward();
} 

// Do a full turn
void turnAround() {
#ifdef DEBUG
  Serial.println("turnAround()");
#endif

#ifdef OLED
  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(0,0);
  display.print("Around!");
  display.display();
#endif

  motorBForward();
  motorDForward();
  motorAReverse();
  motorCReverse();
  motorASpeed(speedSet+MAX_SPEED_OFFSET);      
  motorCSpeed(speedSet+MAX_SPEED_OFFSET);
   
  // run motors this way for 1700mS
  delay(1700);
  stop();

  motorBForward();
  motorDForward();
  motorAForward();
  motorCForward();
}  

// Brakes on
void stop() {
#ifdef DEBUG
  Serial.println("stop()");
#endif
#ifdef OLED
  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(0,0);
  display.print("Stop!");
  display.display();
#endif
  motorASpeed(0);
  motorBSpeed(0);
  motorCSpeed(0);
  motorDSpeed(0);
  motorAStop();
  motorBStop();
  motorCStop();
  motorDStop();
}

// Move forwards.
void forward() {
#ifdef DEBUG
  Serial.println("forward()");
#endif

#ifdef OLED
  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(0,0);
  display.print("Forward!");
  display.display();
#endif

    speedSet = MAX_SPEED;
    
    motorAForward();
    motorBForward();
    motorCForward();
    motorDForward();
    motorASpeed(speedSet);
    motorBSpeed(speedSet);
    motorCSpeed(speedSet);
    motorDSpeed(speedSet);
}

// Reverse
void backward() {
#ifdef DEBUG
  Serial.println("backward()");
#endif

#ifdef OLED
  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(0,0);
  display.print("Reverse!");
  display.display();
#endif

  speedSet = MAX_SPEED;
  motorAReverse();
  motorBReverse();
  motorCReverse();
  motorDReverse();
  motorASpeed(speedSet);
  motorBSpeed(speedSet);
  motorCSpeed(speedSet);
  motorDSpeed(speedSet);
}

// Motor abstraction routines.
//
// Make changes below to support different types of motors
void motorAForward() {
#ifdef DEBUG
  Serial.println("MotorAForward()");
#endif  
  digitalWrite(MOTOR_A_IN_1, HIGH);
  digitalWrite(MOTOR_A_IN_2, LOW);
}

void motorAReverse() {
#ifdef DEBUG
  Serial.println("MotorAReverse()");
#endif  
  digitalWrite(MOTOR_A_IN_1, LOW);
  digitalWrite(MOTOR_A_IN_2, HIGH);
}

void motorAStop() {
#ifdef DEBUG
  Serial.println("MotorAStop()");
#endif  
  digitalWrite(MOTOR_A_IN_1, LOW);
  digitalWrite(MOTOR_A_IN_2, LOW);
}

void motorASpeed(int val) {
#ifdef DEBUG
  Serial.println("MotorASpeed(" + String(val) + ")");
#endif  
  analogWrite(MOTOR_A_ENABLE, val);
}

void motorBForward() {
#ifdef DEBUG
  Serial.println("MotorBForward()");
#endif  
  digitalWrite(MOTOR_B_IN_1, HIGH);
  digitalWrite(MOTOR_B_IN_2, LOW);
}

void motorBReverse() {
#ifdef DEBUG
  Serial.println("MotorBReverse()");
#endif  
  digitalWrite(MOTOR_B_IN_1, LOW);
  digitalWrite(MOTOR_B_IN_2, HIGH);
}

void motorBStop() {
#ifdef DEBUG
  Serial.println("MotorBStop()");
#endif  
  digitalWrite(MOTOR_B_IN_1, LOW);
  digitalWrite(MOTOR_B_IN_2, LOW);
}

void motorBSpeed(int val) {
#ifdef DEBUG
  Serial.println("MotorBSpeed(" + String(val) + ")");
#endif  
  analogWrite(MOTOR_B_ENABLE, val);
}

void motorCForward() {
#ifdef DEBUG
  Serial.println("MotorCForward()");
#endif  
  digitalWrite(MOTOR_C_IN_1, HIGH);
  digitalWrite(MOTOR_C_IN_2, LOW);
}

void motorCReverse() {
#ifdef DEBUG
  Serial.println("MotorCReverse()");
#endif  
  digitalWrite(MOTOR_C_IN_1, LOW);
  digitalWrite(MOTOR_C_IN_2, HIGH);
}

void motorCStop() {
#ifdef DEBUG
  Serial.println("MotorCStop()");
#endif  
  digitalWrite(MOTOR_C_IN_1, LOW);
  digitalWrite(MOTOR_C_IN_2, LOW);
}

void motorCSpeed(int val) {
#ifdef DEBUG
  Serial.println("MotorCSpeed(" + String(val) + ")");
#endif  
  analogWrite(MOTOR_C_ENABLE, val);
}

void motorDForward() {
#ifdef DEBUG
  Serial.println("MotorDForward()");
#endif  
  digitalWrite(MOTOR_D_IN_1, HIGH);
  digitalWrite(MOTOR_D_IN_2, LOW);
}

void motorDReverse() {
#ifdef DEBUG
  Serial.println("MotorDReverse()");
#endif  
  digitalWrite(MOTOR_D_IN_1, LOW);
  digitalWrite(MOTOR_D_IN_2, HIGH);
}

void motorDStop() {
#ifdef DEBUG
  Serial.println("MotorDStop()");
#endif  
  digitalWrite(MOTOR_D_IN_1, LOW);
  digitalWrite(MOTOR_D_IN_2, LOW);
}

void motorDSpeed(int val) {
#ifdef DEBUG
  Serial.println("MotorDSpeed(" + String(val) + ")");
#endif  
  analogWrite(MOTOR_D_ENABLE, val);
}

#ifdef POWER_MON
// Power abstraction routines.
float readBatteryVoltage() {
  float voltage = (analogRead(VBATT) / 1024.0) * VMAX;
#ifdef DEBUG
  Serial.println("Voltage=" + String(voltage));
#endif
#ifdef OLED
  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(0,0);
  display.print("Volt: " + String(voltage));
  display.display();
#endif
  return voltage;
}

float readBatteryCurrent() {
  // Assume an ACS712 current sensor (66mV/A output)
  float Reading = (float)0;
  Reading = (float)analogRead(IBATT) / (float)1024 * (float)VREF * 0.066;
#ifdef DEBUG
  Serial.println("Current=" + String(Reading));
#endif
#ifdef OLED
  display.clearDisplay();
  display.setTextSize(2);
  display.setCursor(0,0);
  display.print("Amp: " + String(Reading));
  display.display();
#endif
  return Reading;
}

float readBatteryPower() {
  return readBatteryVoltage() * readBatteryCurrent();
}
#endif

#ifdef NRF24L01
void bootRadio() {
  radio.begin();
  // Set to minimum power
  radio.setPALevel(RF24_PA_MIN);
  radio.stopListening();
  radio.openWritingPipe(pipes[0]);
  radio.txStandBy();
}
#endif

#ifdef BURN_IN
// Cycle through each motor in turn
void cycleMotor() {
#ifdef DEBUG
  Serial.println("MotorA Test");
#endif

#ifdef OLED
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0,0);
  display.print("Motor A Test");
  display.display();
#endif

  motorAStop();
  motorASpeed(MAX_SPEED);
  motorAForward();
  delay(2000);
  motorASpeed(0);
  motorAStop();
  motorASpeed(MAX_SPEED);
  motorAReverse();
  delay(2000);
  motorASpeed(0);
  motorAStop();

#ifdef DEBUG
  Serial.println("MotorB Test");
#endif

#ifdef OLED
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0,0);
  display.print("Motor B Test");
  display.display();
#endif

  motorBStop();
  motorBSpeed(MAX_SPEED);
  motorBForward();
  delay(2000);
  motorBSpeed(0);
  motorBStop();
  motorBSpeed(MAX_SPEED);
  motorBReverse();
  delay(2000);
  motorBSpeed(0);
  motorBStop();

#ifdef DEBUG
  Serial.println("MotorC Test");
#endif

#ifdef OLED
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0,0);
  display.print("Motor C Test");
  display.display();
#endif

  motorCStop();
  motorCSpeed(MAX_SPEED);
  motorCForward();
  delay(2000);
  motorCSpeed(0);
  motorCStop();
  motorCSpeed(MAX_SPEED);
  motorCReverse();
  delay(2000);
  motorCSpeed(0);
  motorCStop();

#ifdef DEBUG
  Serial.println("MotorD Test");
#endif

#ifdef OLED
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0,0);
  display.print("Motor D Test");
  display.display();
#endif

  motorDStop();
  motorDSpeed(MAX_SPEED);
  motorDForward();
  delay(2000);
  motorDSpeed(0);
  motorDStop();
  motorDSpeed(MAX_SPEED);
  motorDReverse();
  delay(2000);
  motorDSpeed(0);
  motorDStop();
}

void cycleServo() {
#ifdef DEBUG
  Serial.println("Servo = 0");
#endif

#ifdef OLED
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0,0);
  display.print("Servo = 0");
  display.display();
#endif

  myservo.write(0);
  delay(500);
#ifdef DEBUG
  Serial.println("Servo = 45");
#endif

#ifdef OLED
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0,0);
  display.print("Servo = 45");
  display.display();
#endif
  myservo.write(45);
  delay(500);
#ifdef DEBUG
  Serial.println("Servo = 90");
#endif
  myservo.write(90);
  delay(500);
#ifdef DEBUG
  Serial.println("Servo = 135");
#endif

#ifdef OLED
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0,0);
  display.print("Servo = 135");
  display.display();
#endif
  myservo.write(135);
  delay(500);
#ifdef DEBUG
  Serial.println("Servo = 180");
#endif

#ifdef OLED
  display.clearDisplay();
  display.setTextSize(1);
  display.setCursor(0,0);
  display.print("Servo = 180");
  display.display();
#endif
  myservo.write(180);
  delay(500);
}
#endif

