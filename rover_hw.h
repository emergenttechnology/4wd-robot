// 4WD Robot using ultrasonics for obstacle detection and avoidance.
//
// This header file contains the hardware definitions


#define TRIG_PIN 7
#define ECHO_PIN 34
#define MAX_DISTANCE 200
#define MAX_SPEED 100
#define MAX_SPEED_OFFSET 00
#define COLL_DIST 30
#define TURN_DIST COLL_DIST + 20
// Back Right
#define MOTOR_A_ENABLE  2
#define MOTOR_A_IN_1    22
#define MOTOR_A_IN_2    24
// Back Left
#define MOTOR_B_ENABLE  3
#define MOTOR_B_IN_1    26
#define MOTOR_B_IN_2    28
// Front Right
#define MOTOR_C_ENABLE  4
#define MOTOR_C_IN_1    30
#define MOTOR_C_IN_2    31
// Front Left
#define MOTOR_D_ENABLE  5
#define MOTOR_D_IN_1    32
#define MOTOR_D_IN_2    33
#define SERVO_PIN       6
// Analog Inputs
#define IBATT           (A0)    // Battery Current sensor
#define VBATT           (A1)    // Battery input voltage.  Resistor scaled at pin
// Radio module
#define RADIO_CE        39
#define RADIO_CSN       40

// Power Condition Management
// Set the values here to suit your components.  These values
// worked on my chassis, but they might not suit yours.

#define VMAX            9.0    // Maximum allowable voltage
#define BR1             3380.0  // R1 = 3K for battery divider.
#define BR2             1189.0  // R2 = 1K for battery divider
#define VREF            5.0     // Internal analog reference


